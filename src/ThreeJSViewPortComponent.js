import { Group, ObjectLoader, PerspectiveCamera, WebGLRenderer } from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

const loader = new ObjectLoader();

export const start = () => {
    var renderer, canvas;
    function init() {
        canvas = document.createElement('canvas');
        canvas.style = `
        position: absolute;
                top: 0;
				left: 0;
				width: 100%;
				height: 100%;
                pointer-events: none;
        `
        document.body.appendChild(canvas);
        renderer = new WebGLRenderer({ canvas: canvas, antialias: true, alpha: true });
        //renderer.setClearColor(0xffffff, 1);
        renderer.setPixelRatio(window.devicePixelRatio);

    }

    function updateSize() {

        const width = canvas.clientWidth;
        const height = canvas.clientHeight;

        if (canvas.width !== width || canvas.height !== height) {

            renderer.setSize(width, height, false);

        }

    }

    function animate() {

        render();
        requestAnimationFrame(animate);

    }

    function render() {

        updateSize();

        canvas.style.transform = `translateY(${window.scrollY}px)`;

        //renderer.setClearColor(0xffffff);
        //renderer.setScissorTest(false);
        renderer.clear();

        //renderer.setClearColor(0xe0e0e0);
        renderer.setScissorTest(true);
        const scenes = document.querySelectorAll('threejs-viewport');
        scenes.forEach(function (element) {
            const scene = element.scene;

            // get its position relative to the page's viewport
            const rect = element.getBoundingClientRect();

            // check if it's offscreen. If so skip it
            if (rect.bottom < 0 || rect.top > renderer.domElement.clientHeight ||
                rect.right < 0 || rect.left > renderer.domElement.clientWidth) {

                return; // it's off screen

            }

            // set the viewport
            const width = rect.right - rect.left;
            const height = rect.bottom - rect.top;
            const left = rect.left;
            const bottom = renderer.domElement.clientHeight - rect.bottom;

            renderer.setViewport(left, bottom, width, height);
            renderer.setScissor(left, bottom, width, height);

            const camera = element.camera;

            camera.aspect = width / height;
            camera.updateProjectionMatrix();

            //console.log(element.controls)

            renderer.render(scene, camera);

        });

    }


    init();
    animate();

}


document.addEventListener('DOMContentLoaded', () => {

    start();
})

// Define the custom element
class ThreeJSViewport extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });

        const style = document.createElement('style');
        style.innerHTML = `
        :host {
            display: block;
            position: relative;
            container-type: inline-size;
            min-height: 300px;
            min-width: 300px;
        }`;
        this.shadowRoot.appendChild(style);
        // Retrieve the source attribute
        const src = this.getAttribute('src');
        // Load and render the Three.js scene from the specified source
        this.scene = new Group();
        this.camera = new PerspectiveCamera(50, 1, 1, 100);
        this.camera.position.z = 10;
        this.controls = new OrbitControls(this.camera, this);
        this.loadScene(src);
    }

    loadScene(src) {
        fetch(src)
            .then(response => response.json())
            .then(sceneData => {
                loader.parse(sceneData, (model) => {
                    this.scene = model;
                })
            })
            .catch(error => console.error('Error loading scene:', error));
    }
}

// Define the custom element in the DOM
customElements.define('threejs-viewport', ThreeJSViewport);