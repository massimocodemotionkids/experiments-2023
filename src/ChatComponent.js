import { Peer } from "peerjs";

class ChatComponent extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = `
        <style>
            /* Stili per la chat-container e altri elementi */
        </style>
        <div id="chat-container">
            <ul id="chat-messages"></ul>
        </div>

        <div id="message-container">
            <input type="text" id="message-input" placeholder="Type your message...">
            <button id="send-button">Send</button>
        </div>`;

        // Inizializzazione del Peer
        this.peer = new Peer();
        this.peer.on('open', (id) => {
            //console.log('My peer ID is: ' + id);
            this.masterId = id; // Memorizziamo l'ID del Master quando è disponibile
            const masterIdSpan = document.querySelector('#master-id');

            const masterInfo = document.querySelector('#master-info');
            const playerInfo = document.querySelector('#player-info');
            const userTypeRadios = document.querySelectorAll('input[name="userType"]');
            if (masterIdSpan) {
                masterIdSpan.textContent = id;
            }

            if (userTypeRadios[0].checked) {
                masterInfo.style.display = 'block';
                playerInfo.style.display = 'none';
            } else {
                masterInfo.style.display = 'none';
                playerInfo.style.display = 'block';
            }
            this.connections = [];

            this.peer.on('connection', (conn) => {
                this.connections.push(conn);

                conn.on('data', (data) => {
                    this.displayMessage(data);
                });
                //console.log(conn)
            });

        });

        // Gestione dell'invio dei messaggi
        this.shadowRoot.getElementById('send-button').addEventListener('click', () => this.sendMessage());



    }

    // Funzione per inviare messaggi
    sendMessage() {
        const messageInput = this.shadowRoot.getElementById('message-input');
        const message = messageInput.value;
        this.displayMessage(message); // Mostra il messaggio anche nella tua chat
        if (this.conn) {
            this.conn.send(message);
        } else {
            for (let connection of this.connections) {
                connection.send(message);
            }
        }
        messageInput.value = '';
    }

    // Funzione per visualizzare i messaggi nella chat
    displayMessage(message) {
        const chatMessages = this.shadowRoot.getElementById('chat-messages');
        const li = document.createElement('li');
        li.textContent = message;
        chatMessages.appendChild(li);
    }



    // Funzione per inizializzare la connessione con l'ID del Master
    initializeWithMasterId(masterId) {
        // Connessione al Master
        this.conn = this.peer.connect(masterId);
        const welcomeDialog = document.querySelector('#welcome');
        welcomeDialog.close();
        this.conn.on('data', (data) => {
            console.log("DATA", data)
            this.displayMessage(data);
        });
        //this.conn.send('Connected as Player');
        // conn.on('open', () => {

        //     console.log(masterId)

        //     // Esempio: invio un messaggio al Master appena ci connettiamo
        //     
        // });
    }
}

customElements.define('chat-component', ChatComponent);