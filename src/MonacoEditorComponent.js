// import JSONWorker from 'url:monaco-editor/esm/vs/language/json/json.worker.js';
// import CSSWorker from 'url:monaco-editor/esm/vs/language/css/css.worker.js';
// import HTMLWorker from 'url:monaco-editor/esm/vs/language/html/html.worker.js';
import TSWorker from 'url:monaco-editor/esm/vs/language/typescript/ts.worker.js';
import EditorWorker from 'url:monaco-editor/esm/vs/editor/editor.worker.js';

import * as monaco from 'monaco-editor/esm/vs/editor/editor.main.js';

self.MonacoEnvironment = {
    getWorkerUrl: function (moduleId, label) {
        // if (label === 'json') {
        //     return JSONWorker;
        // }
        // if (label === 'css' || label === 'scss' || label === 'less') {
        //     return CSSWorker;
        // }
        // if (label === 'html' || label === 'handlebars' || label === 'razor') {
        //     return HTMLWorker;
        // }
        if (label === 'typescript' || label === 'javascript') {
            return TSWorker;
        }
        return EditorWorker;
    }
};


// document.addEventListener('DOMContentLoaded', () => {
//     const value = `function hello() {
//         alert('Hello world!');
//     }`;

//     // Hover on each property to see its docs!
//     const myEditor = monaco.editor.create(document.getElementById("container"), {
//         value,
//         language: "javascript",
//         automaticLayout: true,
//         fontSize: 32,
//         lineNumbersMinChars: 3,

//     });

//     console.log(myEditor.getValue())
// });

class MonacoEditor extends HTMLElement {
    constructor() {
        super();
        this.value = this.getAttribute('value') || 'var hello = "world"';
        this.language = this.getAttribute('language') || 'javascript';
        this.fontSize = parseInt(this.getAttribute('font-size')) || 32;
        this.lineNumbersMinChars = parseInt(this.getAttribute('line-numbers-min-chars')) || 3;
        this.innerHTML = `
        <style>
        #container {
                width: 100%;
                height: 100%;
        }
        </style>
        <div id="container"></div>
    `;
    this.style="display: block; width: 100vw; height: 30vh; position: absolute; bottom: 0"
        this.initializeEditor();
        // // Initialize MonacoEnvironment
        // self.MonacoEnvironment = {
        //     getWorkerUrl: function (moduleId, label) {
        //         if (label === 'typescript' || label === 'javascript') {
        //             return 'url:monaco-editor/esm/vs/language/typescript/ts.worker.js';
        //         }
        //         return 'url:monaco-editor/esm/vs/editor/editor.worker.js';
        //     }
        // };
    }

    connectedCallback() {
        this.render();
    }

    render() {
   
    }

    initializeEditor() {
        const container = this.querySelector('#container');

        // Hover on each property to see its docs!
        this.editor = monaco.editor.create(container, {
            value: this.value,
            language: this.language,
            fontSize: this.fontSize,
            lineNumbersMinChars: this.lineNumbersMinChars,
            theme: "vs-dark",
        });
        
        
    }

    disconnectedCallback() {
        this.editor.dispose();
    }

    static get observedAttributes() {
        return ['value', 'language', 'font-size', 'line-numbers-min-chars'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (oldValue !== newValue) {
            switch (name) {
                case 'value':
                    this.value = newValue;
                    break;
                case 'language':
                    this.language = newValue;
                    break;
                case 'font-size':
                    this.fontSize = parseInt(newValue) || 14;
                    break;
                case 'line-numbers-min-chars':
                    this.lineNumbersMinChars = parseInt(newValue) || 3;
                    break;
            }

            this.editor.dispose();
            this.initializeEditor();
        }
    }
}

customElements.define('monaco-editor', MonacoEditor);
