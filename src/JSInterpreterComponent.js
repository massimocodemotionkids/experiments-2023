// JSInterpreterComponent.js
import Interpreter from 'js-interpreter';
import * as Babel from '@babel/standalone';
class JSInterpreterComponent extends HTMLElement {
  constructor() {
    super();

    this.running = [];

    this.attachShadow({ mode: 'open' });
    this.code = `
      <style>
        :host {
          display: grid;
          position: absolute;
          inset: 10vw;
        }

        .code {
          background-color: #0006;
          color: white;
          font-family: system-ui;
          overflow: hidden;
          position: relative;
          white-space: pre-wrap;
          width: 100%;
          height: 100%;
          border: none;
          padding: 2rem;
          box-sizing: border-box;
        }

        .run-button, .stop-button {
          position: absolute;
          bottom: 1rem;
          right: 1rem;
          background-color: #0006;
          color: white;
          padding: 1rem;
          border:0;
          cursor: pointer;
        }

        .stop-button {
          left: 1rem;
          right: unset;
        }

      </style>
      <div class="code" spellcheck="false" contenteditable="true">
      ${this.textContent}
      </div>
      <button class="stop-button">Stop</button>
      <button class="run-button">Run</button>
    `;
    this.shadowRoot.innerHTML = this.code;
    this.interpreter = new Interpreter('', this.initMyNativeAPI.bind(this));
    this.setupListeners();
  }

  connectedCallback() {
    this.runCode();
  }

  initMyNativeAPI(interpreter, globalObject) {
    const alertWrapper = (text) => {
      window.console.log(arguments.length ? text : '');
    };
    interpreter.setProperty(globalObject, 'alert', interpreter.createNativeFunction(alertWrapper));

    const promptWrapper = (text, callback) => {
      const result = prompt(text);
      callback(result);
    };
    interpreter.setProperty(globalObject, 'prompt', interpreter.createAsyncFunction(promptWrapper));

    const waitWrapper = (seconds, callback) => {
      setTimeout(callback, seconds * 1000);
    }
    interpreter.setProperty(globalObject, 'wait', interpreter.createAsyncFunction(waitWrapper));

  }

  setupListeners() {
    const runButton = this.shadowRoot.querySelector('.run-button');
    runButton.addEventListener('click', () => this.runCode());

    const stopButton = this.shadowRoot.querySelector('.stop-button');
    stopButton.addEventListener('click', () => this.stop());
  }

  run(code, callback) {

    var options = {
      'presets': ['es2015']
    };
    //Code is transformed to a Javascript version compatible with JS-Interpreter
    try {
      code = Babel.transform(code, options).code;
    } catch (error) {
      callback('parse-error', error.message);
      return
    }
    
    console.log(code)
    var myInterpreter = null;
    try {
      myInterpreter = new Interpreter(code, this.initMyNativeAPI);
    } catch (error) {
      if (callback)
        callback('init-error', error.message);

      return;
    }

    myInterpreter.isRunning = true;

    this.running.push(myInterpreter);

    const runner = function () {
      if (myInterpreter.isRunning) {
        var hasError = null;
        var hasMore = false;
        try {
          hasMore = myInterpreter.run();
        } catch (error) {
          hasError = error.message;
        }

        if (hasMore) {
          // Execution is currently blocked by some async call.
          // Try again later.
          setTimeout(runner, 1);
        } else {
          // Program is complete.

          myInterpreter.isRunning = false;
          if (callback)
            callback(hasError ? 'runtime-error' : 'completed', hasError);
        }
      } else {
        if (callback)
          callback('stopped', myInterpreter.stopReason);
      }
    }.bind(this);
    runner();

  }


  runCode() {
    const codeElement = this.shadowRoot.querySelector('.code');
    const code = codeElement.innerText;
    this.run(code, (reason, message) => { console.log(reason, message) })
  }

  isRunning() {
    return this.running.length > 0
  }

  stop(reason = 'component-stop') {
    while (this.running.length > 0) {
      const interpreter = this.running.pop();
      interpreter.isRunning = false;
      interpreter.stopReason = reason;
    }
  }
}

customElements.define('interpreted-script', JSInterpreterComponent);
