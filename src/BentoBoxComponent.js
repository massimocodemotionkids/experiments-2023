// BentoContainer Component
class BentoContainer extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = `
            <style>

            :host {

                --bento-inset: 2rem;
                --bento-radius: 2rem;
                --bento-gap: 1rem;
                --bento-padding: 0.5rem;
                --bento-item-padding: 0.5rem;
                --bento-portrait: "item1 item1 item2"
                    "item1 item1 item3"
                    "item4 item4 item4"
                    "item5 item5 item5";
                --bento-landscape: "item2 item1 item1 item3"
                    "item2 item1 item1 item3"
                    "item5 item4 item4 item4";
            
                font-size: clamp(8px, 1.5vw, 18px);
            
                --texture-knurled: repeating-linear-gradient(105deg, #47474733, #9e9e9e36 2px, #3d3d3d33 2px, #2d2d2d33 4px);
                --texture-striped: repeating-linear-gradient(-45deg, #9e9e9e26, #00000047 10px, #4545451f 10px 20px);
                border-radius: var(--bento-radius);
                gap: var(--bento-gap);
                padding: var(--bento-padding);
                position: absolute;
                inset: var(--bento-inset);
                border: 0.1rem solid #363636;
                display: grid;
                grid-template-areas: var(--bento-landscape);
                background-image: linear-gradient(145deg, #454545, #313131);
                box-shadow: 0.25rem 0.25rem 0.5rem #2e2e2e,
                    -0.25rem -0.25rem 0.5rem #3e3e3e,
                    inset 0 0 0.25rem #fff3;
            
            
            
                font-family: system-ui;
                color: #ff0066;
                text-shadow: 0 0 5px;
            }



            </style>
            <slot></slot>
        `;
    }
}
var currentBentoItemID = 0;
// BentoItem Component
class BentoItem extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = `
            <style>
                :host {
                    border-radius: var(--bento-radius);
                    background-color: #363636;
                    box-shadow: inset 0.25rem 0.25rem 0.5rem #2e2e2e, inset -0.25rem -0.25rem 0.5rem #3e3e3e, 0 0 0.25rem #6666;
                    display: flex;
                    flex-direction: column;

                    grid-area: item${++currentBentoItemID}
                }
                ::slotted(bento-portion) {
                    /* Stili specifici per le portion inserite nello slot */
                    margin: var(--bento-item-padding);
                    padding: calc(var(--bento-radius) * 0.50);
                    border-radius: var(--bento-radius);
                    background-image: linear-gradient(145deg, #454545, #313131);
                    box-shadow: 0.25rem 0.25rem 0.5rem #2e2e2e, -0.25rem -0.25rem 0.5rem #3e3e3e, inset 0 0 0.25rem #fff3;
                }
            </style>
            <slot></slot>
        `;
    }
}


// BentoPortion Component
class BentoPortion extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = `
            <style>
                :host {
                    /* Stili per la portion */
                    border-radius: var(--bento-radius);
                    background-image: var(--texture-dotted); /* Utilizza la tua texture */
                    box-shadow: 0.25rem 0.25rem 0.5rem #2e2e2e, -0.25rem -0.25rem 0.5rem #3e3e3e, inset 0 0 0.25rem #fff3;
                    display: flex;
                    flex-direction: column;
                }
                ::slotted(*) {
                    /* Stili per gli elementi inseriti nello slot */
                    border: 1px solid #fff1;
                    border-radius: var(--bento-radius);
                    padding: var(--bento-item-padding) calc(var(--bento-item-padding) * 3);
                    margin: 0.25rem;
                    box-shadow: inset 0 0 5px #0007;
                }
            </style>
            <slot></slot>
        `;
    }
}

// Registra i Web Components
customElements.define('bento-container', BentoContainer);
customElements.define('bento-item', BentoItem);
customElements.define('bento-portion', BentoPortion);