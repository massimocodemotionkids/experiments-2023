import * as tf from '@tensorflow/tfjs';
import * as speechCommands from '@tensorflow-models/speech-commands';


class SpeechCommandRecognizer extends HTMLElement {
    constructor() {
      super();
      this.attachShadow({ mode: 'open' });
      this.recognizer = null;
    }

    connectedCallback() {
      this.render();
      this.initializeRecognizer();
      this.startRecognition();
    }

    render() {
      this.shadowRoot.innerHTML = `
        <style>
          /* Aggiungi lo stile desiderato per il tuo componente qui */
        </style>
        <div id="output"></div>
      `;
    }

    async initializeRecognizer() {
      this.recognizer = speechCommands.create('BROWSER_FFT');
      await this.recognizer.ensureModelLoaded();
      console.log(this.recognizer.wordLabels());
    }

    startRecognition() {
      this.recognizer.listen(result => {
        const outputDiv = this.shadowRoot.getElementById('output');
        // Trova la parola con lo score più alto
  var maxScoreIndex = 0;
    var bestScore = result.scores[0];
        
        for (let index = 1; index < this.recognizer.wordLabels().length; index++) {
          //console.log(result.scores[index])
          if (result.scores[index] > bestScore) {
            maxScoreIndex = index;
            bestScore = result.scores[index];
          }
        }
  const recognizedWord = this.recognizer.wordLabels()[maxScoreIndex];

  outputDiv.textContent = `Recognized word: ${recognizedWord}`;
  console.log('Scores:', recognizedWord);
      }, {
        includeSpectrogram: false,
        probabilityThreshold: 0.95
      });

      // Stop the recognition in 10 seconds.
      setTimeout(() => this.recognizer.stopListening(), 30e3);
    }
  }

  // Registra il Web Component
  customElements.define('speech-command-recognizer', SpeechCommandRecognizer);