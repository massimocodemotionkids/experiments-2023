const DEFAULT_JOYSTICK_SIZE = 220;
const DEFAULT_JOYSTICK_COLOR = "#dfdfdf";

class Joystick extends HTMLElement {
    constructor() {
        super();
        const joystickSize = this.getAttribute('size') ? this.getAttribute('size') : DEFAULT_JOYSTICK_SIZE;
        const joystickColor = this.getAttribute('color') ? this.getAttribute('color') : DEFAULT_JOYSTICK_COLOR;
        this.attachShadow({ mode: "open" });

        this.shadowRoot.innerHTML = `
        <style>

            :host {
            --joystick-size: ${joystickSize}px;
            position: absolute;
            width: var(--joystick-size);
            height: var(--joystick-size);
            transform: translate(-50%, -50%);
            --joystick-x: 0;
            --joystick-y: 0;
            z-index: 1000;
            }

            .stick-base {
                position: absolute;
                width: var(--joystick-size);
                height: var(--joystick-size);
                transform: translate(-50%, -50%);
                border-radius: 50%;
                background: ${joystickColor};
                box-shadow:  20px 20px 60px #0003,
                            -20px -20px 60px #ffffff;
                touch-action: none;
                top: calc(var(--joystick-size) * 0.5);
                left: calc(var(--joystick-size) * 0.5);
            }

            .stick-hole {
                position: absolute;
                width: calc(var(--joystick-size) * 0.5);
                height: calc(var(--joystick-size) * 0.5);
                transform: translate(-50%, -50%);
                border-radius: 50%;
                background: ${joystickColor};
                box-shadow: inset 18px 18px 36px #0003,
                            inset -18px -18px 36px ${joystickColor};
                touch-action: none;
                top: calc(var(--joystick-size) * 0.5);
                left: calc(var(--joystick-size) * 0.5);
            }

            .stick-ball {
                position: absolute;
                top: calc(var(--joystick-size) * 0.5 + var(--joystick-y));
                left: calc(var(--joystick-size) * 0.5 + var(--joystick-x));
                width: calc(var(--joystick-size) * 0.7);
                height: calc(var(--joystick-size) * 0.7);
                transform: translate(-50%, -50%);
                border-radius: 50%;
                background: linear-gradient(145deg, #ffffff, ${joystickColor});
                box-shadow:  20px 20px 60px #0003,
                            -20px -20px 60px #ffffff;
                touch-action: none;
                transition: top 0.1s ease-in-out, left 0.1s ease-in-out;        
            }

            .stick-ball.pressed {
                transition: none;
            }

        </style>
    `;

        const stickBase = document.createElement("div");
        stickBase.classList.add("stick-base");

        this.shadowRoot.appendChild(stickBase);

        const stickHole = document.createElement("div");
        stickHole.classList.add("stick-hole");

        this.shadowRoot.appendChild(stickHole);
        this.stickBase = stickBase;

        this.digitalPosition = {
            up: false,
            right: false,
            down: false,
            left: false
        }

        const stickBall = document.createElement("div");
        stickBall.classList.add("stick-ball");

        this.shadowRoot.appendChild(stickBall);
        this.stickBall = stickBall;

        this.isPressed = false;
        this.joystickPosition = {
            x: 0,
            y: 0,
        };
        this.style.setProperty("--joystick-x", "0px");
        this.style.setProperty("--joystick-y", "0px");
        this.style.setProperty("--joystick-size", joystickSize + "px");
    }

    connectedCallback() {
        //this.addEventListener('pointerdown', this.handlePointerDown.bind(this));
        //this.addEventListener('pointermove', this.handlePointerMove.bind(this));
        this.addEventListener("pointerdown", this.handlePointerDown.bind(this));
        this.updateLoop();
    }

    updateLoop(elapsed) {
        //console.log(elapsed)
        if (!this.lastUpdate) {
            this.lastUpdate = elapsed;
        }

        const delta = elapsed - this.lastUpdate;

        this.lastUpdate = elapsed;
        requestAnimationFrame(this.updateLoop.bind(this));

        this.dispatchUpdateEvent(this.joystickPosition, delta);
    }

    handlePointerDown(event) {
        event.preventDefault();
        this.updateJoystickPosition(event);
        this.isPressed = true;
        this.stickBall.classList.add("pressed");
        window.addEventListener("pointermove", this.handlePointerMove.bind(this));
        window.addEventListener("pointerup", this.handlePointerUp.bind(this));
    }




    handlePointerMove(event) {

        event.preventDefault();
        if (this.isPressed) {
            this.updateJoystickPosition(event);
        }
    }

    handlePointerUp(event) {
        event.preventDefault();
        this.updateJoystickPosition(event);
        this.isPressed = false;
        this.stickBall.classList.remove("pressed");
        // Resettare la posizione del joystick al centro dopo il rilascio
        this.joystickPosition = {
            x: 0,
            y: 0,
        };

        // Aggiorna le proprietà CSS relative a joystick-x e joystick-y
        this.style.setProperty("--joystick-x", "0px");
        this.style.setProperty("--joystick-y", "0px");
        window.removeEventListener("pointermove", this.handlePointerMove.bind(this));
        window.removeEventListener("pointerup", this.handlePointerUp.bind(this));
    }

    updateJoystickPosition(event) {
        const { clientX, clientY } = event;
        const property = this.style.getPropertyValue('--joystick-size');
        const joystickSize = parseInt(property.replace('px', ''));
        const joystickHalfSize = joystickSize / 2;

        // Calcola la posizione del joystick rispetto al centro
        const x = clientX - this.getBoundingClientRect().left - joystickHalfSize;
        const y = clientY - this.getBoundingClientRect().top - joystickHalfSize;

        // Calcola la distanza dal centro
        const distanceFromCenter = Math.hypot(x, y);

        // Se la distanza è maggiore del raggio, normalizza la posizione
        if (distanceFromCenter > joystickHalfSize * 0.5) {
            const angle = Math.atan2(y, x);
            const normalizedX = 0.5 * joystickHalfSize * Math.cos(angle);
            const normalizedY = 0.5 * joystickHalfSize * Math.sin(angle);

            this.joystickPosition.x = normalizedX;
            this.joystickPosition.y = normalizedY;

            // Aggiorna le proprietà CSS relative a joystick-x e joystick-y durante il movimento
            this.style.setProperty("--joystick-x", `${normalizedX}px`);
            this.style.setProperty("--joystick-y", `${normalizedY}px`);
        } else {
            // La posizione è all'interno del raggio, aggiorna normalmente
            this.joystickPosition.x = x;
            this.joystickPosition.y = y;

            // Aggiorna le proprietà CSS relative a joystick-x e joystick-y durante il movimento
            this.style.setProperty("--joystick-x", `${x}px`);
            this.style.setProperty("--joystick-y", `${y}px`);
        }
    }

    // Funzione per emettere un evento di aggiornamento (personalizzala secondo le tue esigenze)
    dispatchUpdateEvent(position, delta) {

        const detail = {
            delta,
            position: {
                x: parseInt(position.x),
                y: parseInt(position.y),
                left: position.x < 0,
                right: position.x > 0,
                top: position.y < 0,
                bottom: position.y > 0
            }
        }

        const updateEvent = new CustomEvent("joystickupdate", {
            detail
        });

        // Puoi aggiungere l'ascoltatore dell'evento dove necessario nel tuo codice
        this.dispatchEvent(updateEvent);
    }

    static get observedAttributes() {
        return ['size'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'size' && oldValue !== newValue) {
            this.style.setProperty('--joystick-size', `${newValue}px`);
        }
    }
}

customElements.define("custom-joystick", Joystick);
