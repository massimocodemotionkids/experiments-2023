import Interpreter from 'js-interpreter';
import * as Babel from '@babel/standalone';

var myInterpreter;
function initAlert(interpreter, globalObject) {
    var wrapper = function alert(text) {
        return window.alert(arguments.length ? text : '');
    };
    interpreter.setProperty(globalObject, 'alert',
        interpreter.createNativeFunction(wrapper));
}

function parse() {
    var code = document.getElementById('code').value;
    var options = {
        'presets': ['es2015']
    };
    code = Babel.transform(code, options).code;

    alert('Code transpiled to:\n' + code);
    myInterpreter = new Interpreter(code, initAlert);
    disable('');
}

function run() {
    disable('disabled');
    if (myInterpreter.run()) {
        // Async function hit.  There's more code to run.
        setTimeout(run, 100);
    }
}

function disable(disabled) {
    document.getElementById('runButton').disabled = disabled;
}

document.addEventListener('DOMContentLoaded', () => {

    
    const parseButton = document.querySelector('#parseButton');
    const runButton = document.querySelector('#runButton');


    parseButton.addEventListener('click', parse);
    runButton.addEventListener('click', run);
})